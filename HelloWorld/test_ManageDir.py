import unittest
import ManageDir


class MyTestCase(unittest.TestCase):

    def test_reading1(self):
        mail = ManageDir.p.reading1()
        self.assertEqual(mail, 'bob@123')

    def test_delete(self):
        to_delete = ManageDir.p.delete()
        self.assertEqual(to_delete, 'bob')

    def test_writing(self):
        name = ManageDir.p.writing()
        self.assertEqual(name, 'pedro')

    def test_reading(self):
        todos = ManageDir.p.reading()
        self.assertEqual(todos, "{'bob': ('bob@123', '11', 'tijuana')}")


if __name__ == '__main__':
    unittest.main()