
import unittest
import math


class TestPow(unittest.TestCase):
    def test_pow(self):
        result = math.pow(2, 2)
        self.assertEqual(result, 4)
        self.assertNotEqual(result, 7)

if __name__ == '__main__':
    unittest.main()
