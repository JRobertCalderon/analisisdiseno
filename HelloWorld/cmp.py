
# filecmp.cmp() method
import filecmp

file1 = "12.rtf" # Path of first file
file2 = "13.rtf" # Path of second file

def comp (file1,file2):
    comp1 = filecmp.cmp(file1, file2, shallow=True)
    comp2 = filecmp.cmp(file1, file2, shallow=False)
    print (comp1)
    print(comp2)


comp(file1,file2)
