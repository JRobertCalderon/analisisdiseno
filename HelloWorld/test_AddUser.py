import unittest
import AddUser

class MyTestCase(unittest.TestCase):

    def test_reading(self):
        name = AddUser.p.reading()
        self.assertEqual(name, 'bob')

    def test_writing(self):
        name = AddUser.p.writing()
        self.assertEqual(name, 'pedro')


if __name__ == '__main__':
    unittest.main()
