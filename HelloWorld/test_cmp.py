
import unittest
from cmp import file1, file2

class TestCmp(unittest.TestCase):
    def test_cmp(self):
        result1 = '12.rtf'
        result2 = '13.rtf'
        self.assertEqual(result1, file1)
        self.assertEqual(result2, file2)

if __name__ == '__main__':
    unittest.main()
